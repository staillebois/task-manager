.PHONY: help install-dependencies test run

.DEFAULT: help
help:
	@echo "make install-dependencies"
	@echo "       install dependencies, use only once"
	@echo "make test"
	@echo "       run tests"
	@echo "make run"
	@echo "       run project"

install-dependencies:
	pip3 install -r requirements.txt
	pip install tox

test:
	tox

run:
	python -m task_manager_server