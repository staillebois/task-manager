import connexion
import six

from task_manager_server.models.task import Task 
from task_manager_server import util
from task_manager_server.database import Database

def add_task(body): 
    if connexion.request.is_json:
        body = Task.from_dict(connexion.request.get_json()) 
        return Database().add_task(body)

def get_task_by_id(task_id): 
    task = Database().get_task(task_id)
    return __send_result(task)

def update_task(task_id, body): 
    if connexion.request.is_json:
        body = Task.from_dict(connexion.request.get_json())
        task = Database().update_task(task_id, body)
        return __send_result(task)

def delete_task(task_id, api_key=None): 
    task = Database().delete_task(task_id)
    return __send_result(task)

def find_tasks_by_date(start_date, end_date): 
    return Database().find_task_by_date(start_date, end_date)

def find_tasks_by_priority(priority): 
    return Database().find_task_by_priority(tuple(priority))

def find_tasks_by_keywords(keywords):
    return Database().find_task_by_keywords(tuple(keywords))

def __send_result(task):
    if task is not None:
        return task
    else:
        return 'Not Found', 404, {'x-error': 'id not found'}
