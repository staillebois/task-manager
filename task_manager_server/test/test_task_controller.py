# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from task_manager_server.models.task import Task
from task_manager_server.test import BaseTestCase
from task_manager_server import util
import datetime

class TestTaskController(BaseTestCase):
    """TaskController integration test stubs"""

    def test_add_task(self):
        name = "meeting"
        description = "This is the task description"
        priority = "high"

        task = self.__add_task(name, description, priority)

        assert task.name == name
        assert task.description == description
        assert task.priority == priority
        assert task.creation_date == task.last_update

    def test_delete_task(self):
        name = "toDelete"
        description = "This is the task description"
        priority = "low"

        task = self.__add_task(name, description, priority)
        headers = { 
            'api_key': 'api_key_example',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/{task_id}'.format(task_id=task.id),
            method='DELETE',
            headers=headers)
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        deleted_task = Task.from_dict(response.json)
        
        assert deleted_task.name == name
        assert deleted_task.description == description
        assert deleted_task.priority == priority

    def test_delete_task_not_found(self):
        headers = { 
            'api_key': 'api_key_example',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/{task_id}'.format(task_id=560),
            method='DELETE',
            headers=headers)
        self.assert404(response, 'Response body is : ' + response.data.decode('utf-8'))

    def test_get_task_by_id(self):
        name = "toGet"
        description = "This is the task description"
        priority = "normal"

        task = self.__add_task(name, description, priority)

        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/{task_id}'.format(task_id=task.id),
            method='GET',
            headers=headers)
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        response_task = Task.from_dict(response.json)
        
        assert response_task.name == name
        assert response_task.description == description
        assert response_task.priority == priority

    def test_get_task_by_id_not_found(self):
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/{task_id}'.format(task_id=56),
            method='GET',
            headers=headers)
        self.assert404(response, 'Response body is : ' + response.data.decode('utf-8'))

    def test_update_task(self):
        name = "toUpdate"
        description = "This is the task description"
        priority = "normal"

        updated_name = "Updated"
        updated_description = "This is the updated task description"
        updated_priority = "low"

        task = self.__add_task(name, description, priority)

        body = {
            "name" : updated_name,
            "description" : updated_description,
            "priority" : updated_priority
        }
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/{task_id}'.format(task_id=task.id),
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        response_task = Task.from_dict(response.json)
        
        assert task.id == response_task.id
        assert response_task.name == updated_name
        assert response_task.description == updated_description
        assert response_task.priority == updated_priority
        assert response_task.last_update >= response_task.creation_date

    def test_find_tasks_by_date(self):
        name = "meeting"
        description = "This is the task description"
        priority = "low"

        task = self.__add_task(name, description, priority)

        start_date = task.creation_date - datetime.timedelta(0,1)
        end_date = task.creation_date + datetime.timedelta(0,1)

        query_string = [('start_date', start_date), ("end_date", end_date)]
        headers = { 
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/findByDate',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        response_tasks = util._deserialize_list(response.json, Task)

        assert task in response_tasks

    def test_find_tasks_by_priority(self):
        name = "meeting"
        description = "This is the task description"
        priority = "low"

        task = self.__add_task(name, description, priority)

        query_string = [('priority', 'low')]
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/findByPriority',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        response_tasks = util._deserialize_list(response.json, Task)

        assert task in response_tasks

    def test_find_tasks_by_wrong_priority(self):
        query_string = [('priority', 'test')]
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/findByPriority',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert400(response, 'Response body is : ' + response.data.decode('utf-8'))

    def __add_task(self, name, description, priority):
        body = {
            "name" : name,
            "description" : description,
            "priority" : priority
        }
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task',
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        return Task.from_dict(response.json)

    def test_find_tasks_by_keywords(self):
        keyword1 = "SpecialName"
        keyword2 = "SpecialWord"

        name1 = keyword1
        description1 = "This is the task description"
        priority1 = "low"

        task1 = self.__add_task(name1, description1, priority1)

        name2 = "name"
        description2 = "This is the task description with " + keyword2
        priority2 = "high"

        task2 = self.__add_task(name2, description2, priority2)

        query_string = [('keywords', keyword1 + "," + keyword2)]
        headers = { 
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/task/findByKeywords',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response, 'Response body is : ' + response.data.decode('utf-8'))
        response_tasks = util._deserialize_list(response.json, Task)

        assert task1 in response_tasks
        assert task2 in response_tasks

if __name__ == '__main__':
    unittest.main()
