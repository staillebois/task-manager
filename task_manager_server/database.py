import threading
from datetime import datetime
from functools import lru_cache

class Database(object):
    """NOTE: This class is a Singleton that represent an 'in memory' Database
    """

    _instance = None    
    _lock = threading.Lock()
    _id = 0
    _tasks = {}

    def __new__(cls):
        if Database._instance is None:
            with Database._lock:
                if Database._instance is None:
                    Database._instance = super(Database, cls).__new__(cls)
        return Database._instance

    def add_task(self, task):
        now = datetime.now()
        Database._id += 1
        task.id = Database._id
        task.creation_date = now.strftime("%Y-%m-%d %H:%M:%S")
        task.last_update = now.strftime("%Y-%m-%d %H:%M:%S")
        Database._tasks[Database._id] = task
        return task

    @lru_cache(128)
    def get_task(self, id):
        if id in Database._tasks:
            return Database._tasks[id]

    def update_task(self, id, updated_task):
        if id in Database._tasks:
            now = datetime.now()
            task = Database._tasks[id]
            task.name = updated_task.name
            task.priority = updated_task.priority
            task.description = updated_task.description
            task.last_update = now.strftime("%Y-%m-%d %H:%M:%S")
            return Database._tasks[id]

    def delete_task(self, id):
        if id in Database._tasks:
            task = Database._tasks[id]
            del Database._tasks[id]
            return task

    @lru_cache(128)
    def find_task_by_date(self, start_date, end_date):
        tasks = []
        for id in Database._tasks:
            task = Database._tasks[id]
            if start_date <= task.creation_date <= end_date or start_date <= task.last_update <= end_date:
                tasks.append(task)
        return tasks

    @lru_cache(128)
    def find_task_by_priority(self, priorities):
        tasks = []
        for id in Database._tasks:
            task = Database._tasks[id]
            if task.priority in priorities:
                tasks.append(task)
        return tasks
    
    @lru_cache(128)
    def find_task_by_keywords(self, keywords):
        tasks = []
        for id in Database._tasks:
            task = Database._tasks[id]
            for keyword in keywords:
                if self.__find_in_task(keyword, task):
                    tasks.append(task)
        return tasks

    def __find_in_task(self, keyword, task):
        return keyword in task.name or keyword in task.description or keyword in task.priority or keyword in task.creation_date or keyword in task.last_update