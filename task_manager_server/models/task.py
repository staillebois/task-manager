from __future__ import absolute_import
from datetime import date, datetime 

from typing import List, Dict 

from task_manager_server.models.base_model_ import Model
from task_manager_server import util


class Task(Model):

    def __init__(self, id=None, name=None, creation_date=None, last_update=None, priority=None, description=None):

        self.openapi_types = {
            'id': int,
            'name': str,
            'creation_date': datetime,
            'last_update': datetime,
            'priority': str,
            'description': str
        }

        self.attribute_map = {
            'id': 'id',
            'name': 'name',
            'creation_date': 'creation_date',
            'last_update': 'last_update',
            'priority': 'priority',
            'description': 'description'
        }

        self._id = id
        self._name = name
        self._creation_date = creation_date
        self._last_update = last_update
        self._priority = priority
        self._description = description

    @classmethod
    def from_dict(cls, dikt) -> 'Task':
        return util.deserialize_model(dikt, cls)

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def creation_date(self):
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        self._creation_date = creation_date

    @property
    def last_update(self):
        return self._last_update

    @last_update.setter
    def last_update(self, last_update):
        self._last_update = last_update

    @property
    def priority(self):
        return self._priority

    @priority.setter
    def priority(self, priority):
        self._priority = priority

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description
